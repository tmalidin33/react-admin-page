import React from "react";
import {
	List,
	Datagrid,
	TextField,
	ReferenceField,
	EditButton,
	Edit,
	ReferenceInput,
	SelectInput,
	SimpleForm,
	TextInput,
	Create,
	Filter,
} from "react-admin";

export const PostList = (props) => (
	<List filters={<PostFilter />} {...props}>
		<Datagrid>
			<TextField source="id" />
			<ReferenceField source="userId" reference="users">
				<TextField source="name" />
			</ReferenceField>
			<TextField source="title" />
			<EditButton />
		</Datagrid>
	</List>
);
export const PostEdit = (props) => (
	<Edit {...props}>
		<SimpleForm>
			<TextInput disabled source="id" />
			<ReferenceInput source="userId" reference="users">
				<SelectInput optionText="name" />
			</ReferenceInput>
			<TextInput source="title" />
			<TextInput multiline source="body" />
		</SimpleForm>
	</Edit>
);

export const PostCreate = (props) => (
	<Create {...props}>
		<SimpleForm>
			<ReferenceInput source="userId" reference="users">
				<SelectInput optionText="name" />
			</ReferenceInput>
			<TextInput source="title" />
			<TextInput multiline source="body" />
		</SimpleForm>
	</Create>
);

export const PostFilter = (props) => (
	<Filter {...props}>
		<TextInput label="Search Post" source="q" alwaysOn />
		<ReferenceInput
			label="User"
			source="userId"
			reference="users"
			allowEmpty
		>
			<SelectInput optionText="name" />
		</ReferenceInput>
	</Filter>
);
